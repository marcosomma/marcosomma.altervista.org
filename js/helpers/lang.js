define([
    'settings',
], function( Settings ) {

	return {

        get_lang: function(model) {
            var lang = Settings.lang
            var lang_model = (lang != 0) ? model.attributes.es : model.attributes.en;

            return lang_model
        }, 

	}
});