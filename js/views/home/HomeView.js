define([
    'jquery',
    'backbone',
    'helpers/lang',
    'text!templates/home/template.html'
], function( $, Backbone, Lang, Template ){

    var HomeView = Backbone.View.extend({

        events: {
        },

        initialize: function() {
            this.render();
        },

        render: function() {
            var model = Lang.get_lang(this.model);
            var t = _.template( Template, model);
            this.$el.html(t);
        }, 

        onCloseView: function() {
            
        }

    });

    return HomeView;
});