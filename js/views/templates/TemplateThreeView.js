define([
    'jquery',
    'backbone',
    'helpers/lang',
    'text!templates/templates/t3/template.html'
], function( $, Backbone, Lang, Template ){

    var TemplateThreeView = Backbone.View.extend({

        events: {
        },

        initialize: function() {
            var that = this;
            this.render();
            this.on_click_menu_ref = _.bind( this.on_click_menu, this );
            this.buttons = $('#template-three nav ul li');
            this.templates = $('#template-three #footer .templates');
            this.header_height = $("#header").height();
            this.header_position = $("#header").position().top;

            this.activate_btn()

            $.each( this.templates, function( key, value ) {
                var element = $(value);
                element.on('click', that.on_click_template);
            })
        },

        render: function() {
            var model = Lang.get_lang(this.model);
            var t = _.template( Template, model );
            this.$el.html(t);
        }, 

        activate_btn: function(){
            var that = this;
            $.each( this.buttons, function( key, value ) {
                var element = $(value);
                element.on('click', that.on_click_menu_ref);
            })
        },

        on_click_template: function(e){
            var myUrl = window.location.origin + "/#" + e.currentTarget.id;

            window.location.replace(myUrl);
        },

        on_click_menu: function(e){
            var that = this; 
            var div = $("#content-section");
            var pos;
            var def_pos;

            switch(e.currentTarget.id){
                case 'nav-home':
                    pos = $("#header").parent().parent().position().top;
                    def_pos = pos;
                    break;
                case 'nav-work':
                    pos = $("#work").parent().parent().position().top;
                    def_pos = pos + this.header_height + this.header_position + 10;
                    break;
                case 'nav-portfolio':
                    pos = $("#portfolio").parent().parent().position().top;
                    def_pos = pos + this.header_height + this.header_position + 10;
                    break;
                case 'nav-contact':
                    pos = $("#contact").parent().parent().position().top;
                    def_pos = pos + this.header_height + this.header_position + 10;
                    break;
            }

            div.animate({ scrollTop: def_pos }, 'slow', 'swing');

        },



        onCloseView: function() {
            
        }

    });

    return TemplateThreeView;
});