define([
    'jquery',
    'backbone',
    'helpers/lang',
    'text!templates/templates/t2/template.html'
], function( $, Backbone, Lang, Template ){

    var TemplateTwoView = Backbone.View.extend({

        events: {
        },

        initialize: function() {
            var that = this;
            this.render();
            this.on_click_menu_ref = _.bind( this.on_click_menu, this );
            this.buttons = $('#template-two nav ul li');
            this.templates = $('#template-two #footer .templates');

            this.sections = $('#template-two #content div');

            this.activate_btn()

            $.each( this.sections, function( key, value ) {
                var element = $(value);
                if (!element.hasClass('active')){
                    element.addClass('hidden');
                } else {
                    element.removeClass('hidden');
                }
            })


            $.each( this.templates, function( key, value ) {
                var element = $(value);
                element.on('click', that.on_click_template);
            })
        },

        render: function() {
            var model = Lang.get_lang(this.model);
            var t = _.template( Template, model );
            this.$el.html(t);
        }, 

        activate_btn: function(){
            var that = this;
            $.each( this.buttons, function( key, value ) {
                var element = $(value);
                if (!element.hasClass('active')){
                    element.on('click', that.on_click_menu_ref);
                }
            })
        },

        on_click_template: function(e){
            var myUrl = window.location.origin + "/#" + e.currentTarget.id;

            window.location.replace(myUrl);
        },

        on_click_menu: function(e){
            var that = this; 
            var div;

            switch(e.currentTarget.id){
                case 'nav-home':
                    div = that.sections[0];
                    break;
                case 'nav-work':
                    div = that.sections[1];
                    break;
                case 'nav-portfolio':
                    div = that.sections[2];
                    break;
                case 'nav-contact':
                    div = that.sections[3];
                    break;
            }

            this.change_div(div);

            $.each( this.buttons, function( key, value ) {
                var element = $(value);
                element.off('click').removeClass('active');
            })

            $(e.currentTarget).off('click').addClass('active');

        },

        change_div: function(element){
            var that = this;
            var active_element;
            var anim_element = $('#content');
            var inactive_element = $(element);
            
            $.each( that.sections, function( key, value ) {
                var element = $(value);
                if (element.hasClass('active')){
                    active_element = element;
                }
            })

            anim_element.fadeOut(300, function() {
                active_element.removeClass('active').addClass('hidden');
                inactive_element.removeClass('hidden').addClass('active');
                $( document ).scrollTop( 0 );
                anim_element.fadeIn(300, function() {
                    that.activate_btn();
                });
            })
        },



        onCloseView: function() {
            
        }

    });

    return TemplateTwoView;
});