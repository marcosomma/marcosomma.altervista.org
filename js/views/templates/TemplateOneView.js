define([
    'jquery',
    'backbone',
    'helpers/lang',
    'text!templates/templates/t1/template.html'
], function( $, Backbone, Lang, Template ){

    var TemplateOneView = Backbone.View.extend({

        events: {
        },

        initialize: function() {
            var that = this;
            this.render();
            this.on_click_menu_ref = _.bind( this.on_click_menu, this );
            this.loading = $('#template-one #loading');
            this.buttons = $('#template-one nav ul li');
            this.colors = $('#template-one #footer #color-select button');
            this.templates = $('#template-one #footer .templates');

            this.sections = $('#template-one #content div');

            this.activate_btn()

            $.each( this.sections, function( key, value ) {
                var element = $(value);
                if (!element.hasClass('active')){
                    element.addClass('hidden');
                } else {
                    element.removeClass('hidden');
                }
            })

            $.each( this.colors, function( key, value ) {
                var element = $(value);
                element.on('click', that.on_click_color);
            })

            $.each( this.templates, function( key, value ) {
                var element = $(value);
                element.on('click', that.on_click_template);
            })

        },

        render: function() {
            var model = Lang.get_lang(this.model);
            var t = _.template( Template, model );
            this.$el.html(t);
        }, 

        activate_btn: function(){
            var that = this;
            $.each( this.buttons, function( key, value ) {
                var element = $(value);
                if (!element.hasClass('active')){
                    element.on('click', that.on_click_menu_ref);
                }
            })
        },

        on_click_color: function(e){
            var myClass = "template-" + e.currentTarget.id;
            var body = $('#template-one');

            e.currentTarget.blur();
            body.removeClass();
            body.addClass(myClass);

        },

        on_click_template: function(e){
            var myUrl = window.location.origin + "/#" + e.currentTarget.id;

            window.location.replace(myUrl);
        },

        on_click_menu: function(e){
            var that = this; 
            var div;

            switch(e.currentTarget.id){
                case 'nav-home':
                    div = that.sections[0];
                    break;
                case 'nav-work':
                    div = that.sections[1];
                    break;
                case 'nav-portfolio':
                    div = that.sections[2];
                    break;
                case 'nav-contact':
                    div = that.sections[3];
                    break;
            }

            this.change_div(div);

            $.each( this.buttons, function( key, value ) {
                var element = $(value);
                element.off('click').removeClass('active');
            })

            $(e.currentTarget).off('click').addClass('active');

        },

        change_div: function(element){
            var that = this;
            var active_element;
            var inactive_element = $(element);
            var active_t;
            var inactive_t = that.get_time(inactive_element.height());
            
            $.each( that.sections, function( key, value ) {
                var element = $(value);
                if (element.hasClass('active')){
                    active_element = element;
                    active_t = that.get_time(element.height());
                }
            })

            active_element.slideUp(active_t, function() {
                active_element.removeClass('active').addClass('hidden');
                window.scrollTo(0,0);
                inactive_element.slideDown(inactive_t, function() {
                    inactive_element.removeClass('hidden').addClass('active');
                    that.activate_btn();
                });
            });
        },

        get_time: function(height){
            var coefficient;
            
            if (height <= 750) {
                coefficient = 60;
            } else if ((height > 750)&&(height <= 1500)) {
                coefficient = 45;
            } else if ((height > 1500) && (height <= 3000)) {
                coefficient = 30;
            } else {
                coefficient = 15;
            }
            
            return( (height/100) * coefficient);
        },

        onCloseView: function() {
            
        }

    });

    return TemplateOneView;
});