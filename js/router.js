define([
    'jquery',
    'underscore',
    'backbone',
    'settings',
    'events/AppEvents',
    'views/home/HomeView',
    'views/templates/TemplateOneView',
    'views/templates/TemplateTwoView',
    'views/templates/TemplateThreeView',
    'models/ResumeModel',
], function( $, _, Backbone, Settings, AppEvents, HomeView, TemplateOneView, TemplateTwoView, TemplateThreeView, Model) {
    
    var AppRouter = Backbone.Router.extend({

        routes: {
            '*actions':    'init',
            // '':            'init',
            // 'template1':   'init',
            // 'template2':   'init',
            // 'template3':   'init',
        },

        init: function() {
            if(!this.model) {
                this.model_inizialize();
                $('#en').on('click', _.bind(this.on_lang_click, this));
                $('#es').on('click', _.bind(this.on_lang_click, this));
                this.flags = $('#choice-lagn div')
            } else {
                this.show_page()
            }
        },

        model_inizialize: function(){
            var that = this;
            this.model = new Model();
            this.model.url = 'resume.json';
            this.model.fetch({
                success: function() {
                    that.model.toJSON();
                    that.show_page();
                }
            });
        },

        on_lang_click: function(e){
            var value = Number(e.currentTarget.getAttribute("data-value"))
            if (value == Settings.lang) return;

            $.each( this.flags, function( key, value ) {
                var flag = $(value);
                flag.removeClass("active");
            })

            Settings.lang = value;
            
            $(e.currentTarget).addClass("active")

            this.show_page()
        },

        show_page: function() {
            var location = window.location.hash
            switch ( location ) {
                case '#template1':
                    new TemplateOneView({ el: '#content-wrapper', model : this.model });
                    return;
                    break;
                case '#template2':
                    new TemplateTwoView({ el: '#content-wrapper', model : this.model });
                    return;
                    break;
                case '#template3':
                    new TemplateThreeView({ el: '#content-wrapper', model : this.model });
                    return;
                    break;
            }

            new HomeView({ el: '#content-wrapper', model : this.model });
        },

        show_404: function() {
            console.log("content not found!");
        }

    });

    var initialize = function(){
        app_router = new AppRouter;
    };

    return { 
        initialize: initialize
    };
});