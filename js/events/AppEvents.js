define([
    'underscore',
    'backbone'
], function(_, Backbone) {

    var AppEvents = {
    	
    };
    _.extend(AppEvents, Backbone.Events);

    return AppEvents;

});