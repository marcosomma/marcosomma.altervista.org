module.exports = function(grunt) {

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-copy');

  var userConfig = require( './build.config.js' );

  var taskConfig = {
    pkg: grunt.file.readJSON('package.json'),

	jshint: {
	  // define the files to lint
	  files: ['gruntfile.js', 'js/**/*.js'],
	  // configure JSHint (documented at http://www.jshint.com/docs/)
	  options: {
	      // more options here if you want to override JSHint defaults
	    globals: {
	      jQuery: true,
	      console: true,
	      module: true
	    }
	  }
	},
	clean: [ 
      '<%= dist_dir %>/templates', 
      '<%= dist_dir %>/*.txt',
    ],
	requirejs: {
		compile: {
			options: {
			    appDir: '<%= app_dir %>',
			    baseUrl: '<%= base_url %>',
			    dir: '<%= dist_dir %>',
			    modules: [
			        {
			            name: 'main'
			        }
			    ],
			    fileExclusionRegExp: /^((r|build|Gruntfile|build.config)\.js)|.git|.gitignore|.styl|Thumbs.db|node_modules$/,
			    optimizeCss: 'standard',
			    removeCombined: true,
			    paths: {
			        jquery: 'libs/jquery',
			        underscore: 'libs/underscore',
			        backbone: 'libs/backbone',
			        text: 'libs/require/text',
			        templates: '../templates'
			    },
			    shim: {
			        underscore: {
			            exports: '_'
			        },
			        backbone: {
			            deps: [
			                'underscore',
			                'jquery'
			            ],
			            exports: 'Backbone'
			        }
			    }
			}
		}
	},
	copy: {
	  main: {
	    cwd: '<%= dist_dir %>/',
	    expand: true,
	    src: '**',
	    dest: '<%= remote_dev_dir %>/',
	  },
	},
  };

  grunt.initConfig( grunt.util._.extend( taskConfig, userConfig ) );

  grunt.registerTask('hints', ['jshint']);
  grunt.registerTask('build', ['requirejs', 'clean']);
  grunt.registerTask('remote', ['build','copy']);
};